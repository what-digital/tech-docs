Those projects are usually using python2.

It also requires `ansible` less than 2 version. On ubuntu you can get it with pip by installing it globally `pip install 'ansible < 2'`.

The latest version (2.2+) won't work, try 2.0.4 or 2.1.5.

# Remote `media` directory

If you don't want to store ~50 GB on your dev machine, you can try to mount the `media` directory through `sshfs`.

1. You have to make sure that whatever editor / IDE you are using is not indexing the media folder (in pycharm, exclude the media folder)
2. Install the `sshfs` pakcage (`brew install sshfs`/`sudo apt install sshfs` and follow instructions)
3. Mount the media directory via `sudo sshfs -o allow_other,defer_permissions www-data@{project_name}-{project_branch}.notch-interactive.com:/home/www-data/sites/{project_name}-{project_branch}/media /{path_to_your_projects}/{project_name}/media`,
where `project_branch` might be `stage` for example.

Note: for some time at the beginning the page will be very slow. But after 30 minutes the page speed went back to normal on Mario's mac. Possibly it's better to setup sshfs inside the vagrant box for better initial performance.

You can unmount anytime by `sudo umount /{path_to_your_projects}/{project_name}/media`

Also the ssh connection might be closed because of no activity from your side, in order to avoid it you can add to your `~/.ssh/config` the following line:
```
ServerAliveInterval 5
```

# Vagrant specific issues

### postgresql database errors

Can be either
> msg: unable to connect to database: could not connect to server: No such file or directory

Or
> `No PostgreSQL clusters exist; see "man pg_createcluster"` error during the execution of `vagrant up`

That's ridiculous but the issues is about the locales settings (for details see [that question](https://dba.stackexchange.com/questions/50906)). Looks like it happened to me because during my initial OS installation the UA locale was set based on my IP, and our ansible config doesn't except that locale in the settings. You need to run `vagrant ssh` and execute the following commands substituting `uk_UA` with whatever locale you're having that problem with.
```
sudo locale-gen en_US.UTF-8
```
Logout from the ssh and run `vagrant ssh`. Repeat the ssh logout and login again. Yes, twice. No kidding. After:
```
sudo update-locale LANG=en_US.UTF-8
sudo update-locale LANGUAGE=en_US.UTF-8
sudo update-locale LC_ALL=en_US.UTF-8
```
If you see any locale error while you're executing the aforementioned commands, try to logout and login again.

Then log out of the ssh shell, login again and run `sudo pg_createcluster 9.3 main --start`. If it return another error consider trying more locale fixing error. Good luck with that.

After you were finally able to make `sudo pg_createcluster 9.3 main --start` you need to restart the provision by running `vagrant provision` since it has to finish the setup.

### `vagrant provision` hangs on `Django - Run deployment command`

That's most likely ok if you can connect to the VM using `vagrant ssh` from another terminal session. No idea why exactly that's happening, but looks like you can interrupt the installation on that step with no consequences.

# OS specific issues

### undefined method `group_by' for nil:NilClass (NoMethodError)

If on running `vagrant up` you see warnings like:
```
/usr/lib/ruby/2.3.0/rubygems/specification.rb:949:in `all=': undefined method `group_by' for nil:NilClass (NoMethodError)
```
And after the installation is finished it throws the following error:
```
Vagrant:
* Unknown configuration section 'trigger'.
```

Try to remove your `vagrant` package and install the latest version from the [vagrant downloads page](https://www.vagrantup.com/downloads.html). For details see the respective [github issue](https://github.com/vagrant-libvirt/vagrant-libvirt/issues/575).
