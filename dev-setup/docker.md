## Intro

In order to be able to set breakpoints and debug the project properly, a pycharm run / debug configuration comes in very handy forces

- running the project (green triangle button in pycharm)
- debugging the project (green bug button in pycharm)


## Setup Steps

### Set up Docker Compose interpreter

<details>
<summary>image</summary>

![](img/add-new-environment-in-pycharm.png)
</details><br>

then

<details>
<summary>image</summary>

![](img/setup-docker-environment.png)
</details>

### enable support for Django

<details>
<summary>image</summary>

![](img/enable-django-support.png)

</details>

### mark src as source root

Any folder that contains modules that are included in the project via INSTALLED_APPS in settings.py needs to be marked as source root as pycharm does not pick up the PYTHONPATH environment variable in the Dockerfile.

<details>
<summary>image</summary>

![](img/mark-as-src-root.png)

</details>

### Add run / debug configuration

You need to make sure that pycharm executes `./manage.py runserver <address>:<port>` the way docker-compose.yml is expecting it.

- pycharm must run the Django development server on the correct internal address and port (default: 0.0.0.0:80)
- on your host system (your computer) you then connect via browser to the correct external address and port (default: 127.0.0.1:8000)

<details>
<summary>image</summary>

![](img/look-up-docker-compose-port-and-address.png)

</details><br>

then:

<details>
<summary>image</summary>

![](img/run-debug-configuration.png)

</details>

## Troubleshooting

Let's be clear - the integration is erroneous as hell the community has plenty of negative feedback about it.

When something broke again - it's normal. You just restart the IDEA and see if it helps. If it doesn't you reset the cache and restart again. If it's still broken - most of the time just accept that it's permanent.

#### External libraries aren't searchable

If several resets of your intellij cache doesn't help - try to downgrade you version until it starts working again.

You can try to delete and recreate the virtualenv (or python SDK) in intellij. Several times.

Sometimes unfixable.

The following doesn't work:
- Delete all docker SDKs (virtualenvs) from pycharm/intellij, otherwise if one of them returns the error - intellij is going to index nothing at all. Even if you're working let's say with apg, but the error is in srgd container.
- Invalidate caches and restart.
- Start from the terminal.
- Add your SDK/virtualenvs again.

#### `Couldn't find 'docker' binary` on Mac

A bug in intellij, you need to ensure that `docker` is in your PATH. It also works when started from the terminal, as `/Applications/IntelliJ\ IDEA.app/Contents/MacOS/idea`. https://youtrack.jetbrains.com/issue/PY-31727

However running it from the terminal will break the git integration, since it's going to start asking for you ssh password in shell every time you do any repository operation. And after you provide the ssh password in the shell, it won't work anyway.

Installing idea using Jetbrains Toolbox might help.

#### Cannot retrieve debug connection

Also possible `ERROR: No such service: pycharm_helpers`. Try running `docker container ls -a | grep phpstorm_helpers_PS | awk '{print $8}' | xargs docker rm` containers. Didn't help me, but helped to some people on stackoverflow - https://stackoverflow.com/questions/49818282

As for now I don't know how to fix it.

#### Changes executed in bash aren't saved

At the moment there's no normal way of handling it.

Don't use `--rm` flag or `docker-compose exec web bash`, both of those commands simply create a new container in parallel.

If you want to edit the virtualenv, you have to add something to the `command` of docker-compose.yml file, that won't stop `web` container, but won't start `runserver`. `tail -F anything` can do. After that you need to `docker-compose exec web bash`, make any changes within the container and call `python manage.py runserver 0.0.0.0:80` yourself within the same session.

Also poratainer allows to ssh into a container, I haven't tried it though.

Sometimes `docker-compose exec web bash` will create a new container. Sometimes it won't. No pattern has been discovered. Full rebuild sometimes helps. Not always. You might need to run additional rebuild with disabled cache in docker compose.

#### pip uninstall doesn't work

Default pip in docker might not be able to uninstall several packages. In order to resolve that you might need to upgrade pip in docker. Add the upgrade to the dockerfiles, otherwise you're going to do it every time you ssh into a new container.

#### packages aren't loaded with error `Cannot find external tool: packaging_tool.py`

No solution.

There're people for whom it happened only after an upgrade from 2018.3, a downgrade might help.

Latest intellij seems to be working.

#### `Run` button doesn't work because: Can't run remote python interpreter: Docker account not specified

No solution.

Sometimes restart & reset help. Sometimes it doesn't.

#### My venv libs in intelij are different from the real libs in docker

Intellij doesn't allow to view the files in docker, it simply copy-pasts the venv directory from the docker into a cache directory on your machine hard drive. In other words two of them can be out of sync and that happens fairly often. Sometimes you might need to reset the cache by pressing `Invalidate Caches / Restart` once per per day or two.

It also causes parsing errors and syntax highlighting errors. There's no solution to that expect restarts and cache invalidation.

#### On Mac docker causes CPU clock increase, overheating and battery discharge in 2-3h instead of 8h, or painfully slow on an older generation

It's a virtual machine in a virtual machine. There's no solution for that.

#### Running another command through IDEA as `migrate` or `makemigrations` stops the running server

No solution.
