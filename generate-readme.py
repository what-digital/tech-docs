import re
from subprocess import check_output


def main():
    """
    Usage:
    
    brew install tree
    python3.7 generate-readme.py
    
    Beware that mac tree messes with unix tree specs somehow. `tree -I '*.md'`
    must match NON markdown files, instead on mac it matches everything.

    And `tree -P '*.md'` which must match markdown files matches non-markdown
    files.

    Intellij debug mode fixes that problem. Somehow.
    """
    read_tree_command: str = '/usr/local/bin/tree -f -P "*.md"'
 
    output_bytes: bytes = check_output(read_tree_command.split(), encoding='utf-8')
    output = str(output_bytes)

    output = output.replace('─ .', '─- ')
    output = output.replace('├──', '    ')
    output = output.replace('│   ', '    ')
    output = output.replace('└──- ', '    - ')
    output = output.replace('└──- ', '    - ')

    output = re.sub(r'^    ', '', output, flags=re.MULTILINE)

    output = re.sub(r'- /(.+)\.md', r'- [\1](/\1.md)', output)
    output = re.sub(r'\[.+/(.+)]', r'[\1]', output)

    output = re.sub(r'^\.\n', '', output, flags=re.MULTILINE)

    output = output.replace('- [README](/README.md)', '')

    output = re.sub(r'[0-9]+ directories, [0-9]+ files', '', output)

    re_for_removing_path_from_dir_names = re.compile(r'-.+/([\w.-]+)$', re.MULTILINE)
    output = re.sub(re_for_removing_path_from_dir_names, r'- \1', output)

    # redundant dirs
    output = re.sub(r'.+- img\n', '', output, flags=re.MULTILINE)
    output = re.sub(r'.+- ssl-certs\n', '', output, flags=re.MULTILINE)
    output = re.sub(r'.+- chain\n', '', output, flags=re.MULTILINE)
    
    footer = """    
    This ToC is automatically generated. For generation instructions 
    look into [generate-readme.py](/generate-readme.py)\n\n
    """.strip()
    output += footer

    with open('README.md', 'w') as readme:
        print(output, file=readme)


if __name__ == '__main__':
    main()
