Before a deploy to the stage, run the `manage.py compilemessages` command and ensure that it compiles without errors. Otherwise the server might stop working.

In order to avoid this issue you have to always fill in the `context` argument. In other words always instead of writing:
```
{% trans "Thank you" %}
```
write:
```
{% trans "Thank you" context 'order-email' %}
```

Same goes for the raw python methods. Don't use `gettext`, use `pgettext`:
```
from django.utils.translation import pgettext

pgettext('order-email', "Thank you")
```

Also keep an eye on the translations that are marked as `fuzzy`, the `collectmessages` command marks this way the potentially wrong translations as `Thank you` -> `Adiós`. That flag means that you have to manually double check the translation in question.

Generate string without soft warps, eg `python manage.py makemessages --no-wrap`. And then enable the soft wrap in your editor. In intellij/pycharm you can configure it by default for *.po files, you can find it in intellij docs.

Never add html tags. Use `blocktrans`. Read how ot use it in the django documentation. If it isn't avoidable - at least move your css classes and other helper code out of blocktrans.

### Caveats

- The translations marked with the `fuzzy` comment should not work, even although they look as they do. I can confirm that they at least don't work after the compilation.
- Don't use aliases for `pgettext` - then `makemessages` won't  collect those strings. In truth django simply matches `_()` the underscores, even if your underscore is aliasing another function altogether - django will try to collect it's content for translation.
- Don't forget to check that it compiles *the correct* translations. We had a problem when everything was translating correctly without the compilation on a dev instance, but after the `compilemessages` command some strings ended up in a completely wrong place. For example if on Spanish a string like `Thank you` was translating to `Gracias` on a dev instance, after the compilation it swapped to `Adiós` all of a sudden.
- if `django-admin {command}` raises an error then `python manage.py {command}` might work instead
