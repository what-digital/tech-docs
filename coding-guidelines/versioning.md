We're using a versioning system similar to semantic versioning, but with one more leading number.

The main reason is that semantic versioning is a great idea that, alas, not suitable for many applications and packages. That's why django doesn't use it, django-cms doesn't use it, even python doesn't.

The issue is simple - developer and the marketing department don't want to release a new major release, eg django 4, every time they change a keyword argument name in a function that nobody uses.

Our system is the following:

```
⌜ indicates either a new marketing release, or a complete rewrite, eg angular 1 and angular 2
|
| ⌜ all other numbers follow semantic versionsing, this is "major release" - any breaking change, even a renamed keyword argument, anything
| |
| | ⌜ a minor release, no breaking changes, only a new feature
| | |
| | | ⌜ a patch release
| | | |
2.5.3.15
```

The versions not ready for production usage are prefixed with a `0`, eg `0.5.3.15`.
