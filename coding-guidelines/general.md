Code writing
================================================================================

In general try to keep your code as "dumb" as possible. We're often tempted to write elegant and intelligent code. But everybody can write elegant code, the art is in writing dead simple code that everybody can easily understand and debug. It's hard to find a project which was ditched because its codebase wasn't elegant enough. It's easy though to find overcomplicated for no reason projects, or even whole languages that start dying because their elegance has a tendency to make them unmaintainable, eg scala.

## Comments

Code documentation documentation should answer *why*, not *what*.

A copy of a reddit comment:

Good naming, types, design, patterns, and data flow can go a long way to requiring less and less documentation. You do not need to comment:
```python
# Returns the house temperature in Celsius
value = get_current_value()
```
if instead you write:
```python
house_temp: Celsius = get_house_temp()
```

Similarly:
```python
# check if temp is within the safety limit 
if temp < 100:
    [...]
```
can be:
```python
is_temp_within_safety_limits = temp < 100
if is_temp_within_safety_limits:
    [...]
```

However, there are things you should consider commenting like: hacks, safety, requirements, intent, alternatives, edge cases, etc.


## Imports

Use absolute imports by default. The modern IDEs are expecting that.

Use Intellij (PyCharm) autocomplete for the imports:
- you can read about `Alt+Enter` import action [here](https://www.jetbrains.com/help/pycharm/creating-imports.html)
- another neat way
    - while you're typing a class name, eg `Blockcha` press the `Ctrl+Space+Space` in order to the import pop up to appear
    - <details><summary>import image</summary>
        
        ![](/coding-guidelines/img/intellij-auto-import.png)
        
        </details>
    - then you can select your class name with your keyboard arrows, press `Enter` and Intellij (PyCharm) will add you import automatically

Configure the settings in IDE:




## Variable Names

### Boolean Types
Use the `is_` prefix for boolean variables, even when it's not valid English.

Variables such as `investment` might mean anything really, eg "*investment amount*", "*investment type*", "*has user invested*", "*is user approved for investment*", "*investment status*", etc. But the variable `is_allowed_to_invest` is an unambiguous true/false question.

Also since javascript and python aren't always able to tell you a variable type, the `is_` prefix will help you to avoid treating a boolean as a string or anything else.

###### Magic Numbers
No magic numbers in the middle of the code. Magic numbers should be documented (write where it is used or what it does) and added in UPPERCASE to the beginning of a file / class or should be in a settings.py file or similar or should be an env variable.

###### Hungarian Notation
In dynamically typed languages the readability can be significantly improved by adding the type (or its abbreviation) to the variable name.
- `error_msg_byte_list` instead of `error_msg` as the later can be taken for a utf-8 string
- `car_queryset` instead of `cars` since a `List` isn't identical to a `QuerySet`
- `html_body_str` instead of `html_body` as the later can be taken for a `BeautifulSoup` instance


## Single Quotes vs Double Quotes

By default use the following guidelines:
- if you're writing a string for a compiler/parser (a enum value, a dict key, ect) - use single quotes
- if you're writing a valid English string that a user is going to read (an error message, a field hint, etc) - use double quotes

<details>
<summary>Explanation</summary>

That convection is often used because when we write valid English, we often need the `'` character in our strings, eg `can't`, `isn't`, etc. Therefore we must either use the double quotes, or escape the single char every time. And in code the usage of `'` is simpler mostly because you don't need to press `shift` every time in order to type it. Also this kind of consistency is rather handy, because it simplifies your code search and regexes.

</details>


## Functions

- Functions that start with `get_` should be indepotent unless stated otherwise.
- Functions should do one thing, and what it is must be clear from its name. If the function is named as `def get_price(product: Product) -> int`, then it shouldn't do anything expect reading the price, no console ouput, no products creation, etc.


## `if` statements

### create explanatory variables

When you have even a mildly complex `if` expression, that can benefit from a comment, consider introducing a variable as `is_has_permissions`. For an example see the `Comments` section above.

### Use Positive Statements
Avoid using negative statements in your `if` statements whenever possible, since they're harder to process cognitively comparing to positive statements ([Nordmeyer, 2014](https://langcog.stanford.edu/papers/NF-cogsci2014.pdf)).

Don't:
```python
if not is_queryset_not_filled:
    pass
```
Do:
```python
if is_queryset_filled:
    pass
```


## List format

Use the following format in all languages, because it's more readable and easier to extend by selecting a row and pressing `CMD+D` or `Ctrl+D`:
```
val list = [
    'value1',
    'value2',
    'value3',
]
val maps = [
    {value1: 'value1'},
    {value2: 'value2'},
    {value3: 'value3'},
]
val maps_nested = [
    {
        value1: 'value1',
        value1: 'value1',
        value1: 'value1',
    },
    {
        value2: 'value2',
        value2: 'value2',
        value2: 'value2',
    },
    {
        value3: 'value3',
        value3: 'value3',
        value3: 'value3',
    },
]
```


## URL trailing slashes

Usually you would append a trailing slash if the urls that you're defining is a category, don't use slashes if its the last item in the hierarchy.

But some frameworks as django enforce appending of trailing slashes by design. There we need to follow their convention because frameworks can be awkwardly dependant on those implicit expectations.


## Requirements Management

Don't add random packages into requirements.in and package.json. On older projects we have requirements conflicts that can take weeks to resolve, simply because the original developers were throwing packages into the requirements one by one until the issue was gone. Or copy pasted 30 redundant requirements from the old project, and even enabled them all, thus making it even harder to figure out whether all those packages were ever needed. 
- If you're adding a package which's name isn't verbose enough, eg `tqdm` - add a comment about where it's used, to let others know when it can be dropped.
- If you're adding a package that might be useless but you don't have the time to check - add a comment about it.
- If you're adding a peer dependency - add it below the parent package with an indent.


## TODO
- Replace Overcomplicated Statements with Explanatory Variables
