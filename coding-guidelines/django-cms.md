### Linking Files
- Always use the filer integration: Use FilerFileField(), FilerImageField(), FilerFolderField() instead of django's FileField()
- Always `use on_delete=models.PROTECT` - we had several accidents where an editor would accidently delete a folder of images that was containing 30GB of images, and drop half the site in the process. In short let's use `PROTECT` as default value for `on_delete` and use `CASCADE` only if there is a solid reason for that 

### Static Files
- always add images to the static folder (normally inside the `frontend` folder), dont hard-code links of files you've uploaded into the django CMS media library. Editors might change or delete such images later which will break your hard-coded links. Always use the `static` tag to reference to images: `{% static 'frontend/global/img/logo.png' %}`

### CMS Placeholders
- CMS placeholders are defined in the HTML templates of CMS pages and as PlaceHolderField() on models
- Try to use as few placeholders as possible. Having many placeholders on a CMS template is confusing for the editor. Use the different width options of the SectionPlugin to organize content into different sections instead.
- Give the placeholders nice names via the corresponding setting in settings.py
- When appropriate remember to limit the available plugins for placehoder using `settings.CMS_PLACEHOLDER_CONF`, you can read [the docs here](http://docs.django-cms.org/en/latest/reference/configuration.html#cms-placeholder-conf).

### URLs
- do not hard-code URLs to CMS pages
- Use django CMS PageField() instead of django URLField(): https://blog.typodrive.com/2019/11/11/django-cms-pagefield/
- Use https://github.com/dreipol/linkit so that editors can link to model detail pages easily

### Hiding plugins

- In parent plugins and CMS placeholders, only allow child plugins that make sense, but limit the editors without a good reason.

### Absolute urls

Also remember that specific fields might require the ability to input an absolute url (eg `/account/login/`), with or without a domain. For now there's no package that would provide the possibility of selecting a page or using a url.

A workaround it to attach a page and configure a custom redirect to it. But that should be avoided whenever possible.

djangocms-redirects has a field that satisfies the technical requirements, but its UX is not highly user-friendly.

### Text-enabled plugins

if you use `text_enabled = True` on a plugin, make sure you have a CharField named `name` so that text the editor has selected before adding the plugin is used as its default value, see here for more info: https://github.com/divio/djangocms-text-ckeditor/issues/528

### On click editing of custom models

If you wrap you plugin in `render_model` django tag you'll be able to edit the original model by double clicking on that html block, eg
```html
{% render_model_block post %}
    <h3>{{ post.title }}</h3>
    <img src="{{ post.image.url }}"/>
{% endrender_model_block %}
```


Plugins
================================================================================

### Architecture

##### Documentation of fieldsets

Every time your plugin does something that your grandmother won't be able to handle right away (= ie most of the time), add a fieldset description to it:

```pythoon
(None, {
    'fields': [...],
    'description': "In order to add popup content add children to this Item plugin.",
})
```

It's also better to keep the default list of `fields`, since otherwise you will keep forgetting to update your hardcoded list of fields in admin.py.
You can inherit the existing list as following:

```
    def get_fieldsets(self, request: HttpRequest, obj: PersonPluginModel = None) -> dict:
        fieldset = super().get_fieldsets(request, obj)
        fieldset[0][1]['description'] = "You can add people plugins inside this list plugin in the structure mode."
        return fieldset
```

If your plugin must be created without any configuration and the editor needs to add children, you must override `CMSPlugin.get_empty_change_form_text`, by default it says `There are no further settings for this plugin. Please press save.`.

##### Documentation of thumbnails

If you've enabled the image thumbnails:
- inform the editor of the image thumbnail size in FilerImageField.help_text, eg "This image will be scaled up or cropped down to 700x450 pixels"
- make sure that you added the focal point support

##### UX

Use as little of of nesting plugins or bootstrap4 plugins as possible. Always prefer simplicity over flexibility. The editors are constantly running into issues by accidentally drag&dropping a plugin in a wrong place and breaking the whole layout. Sometimes they have to pay us to fix that, because they are unable to do it on their own, which indicates a poorly implemented UX.

Don't add css attributes or custom classes in admin anywhere.

##### Slides, Carousels, etc

In any plugin that requires a plugin that renders inside itself people, slides, files - always prefer instead of using child plugins over M2M for slides and other elements. If you use M2M then it isn't possible to sort the child elements in a sane way as it's possible to sort child plugins using native django cms.

##### CKEditor

When you add a text plugin inside your custom plugin and override the text styles - you must add `child_ckeditor_body_css_class` to your CMSPluginBase and then make sure that the content of CKEditor WYSIWYG is styled correctly. You can also you classmethod `get_child_ckeditor_body_css_class` for dynamic classes. For example:

<details>

```python
    @classmethod
    def get_child_ckeditor_body_css_class(cls, plugin: CMSPlugin) -> str:
        instance = SectionPluginModel.objects.get(pk=plugin.pk)
        css_classes = 'section-plugin'
        css_classes += f' section-plugin--bg-{instance.background.value}'
        return css_classes
```

![](/coding-guidelines/img/ckeditor-iframe-styling.png)

</details>

### Headers

Make sure that your header (it might be an image or a slier) show up as the default meta image, eg when you open https://www.facebook.com/sharer/sharer.php?u={your_site}

### Section Plugin

Use Container plugin from djangocms-bootstrap4 instead of the existing Section plugins. You can configure it's available dropdown selects for max width, background and spacing in settings.py, see its constants.py file for examples.

### Naming

As a developer you might often derive the plugin specification from a design document, hence you're usually inclined towards giving content-specific names to the plugins.

For example you might try to name the plugin below as `InstructorPlugin`

<details>

![](https://i.imgur.com/OJl7rpm.png)

</details><br>

Because it's only utilized within this use-case throughout all the design. But that decision limits the editors, because once they decide to use it for demonstration of the students the plugin name is going to become irrelevant.

Hence it's better to prefer generic plugin names, eg in this case `ImageWithCornersAndContentPlugin`.

##### Classes Naming

We're respecting django-cms core conventions.

Children of CMSPluginBase are to be named as `InstructorPlugin`. Children of `models.Model` can be named either as `InstructorPluginModel` or `Instructor`.

### Code Style

- The naming of plugins must follow the [django](/coding-guidelines/code-style/django.md) guidelines.


Common Issues
================================================================================

### Custom Login View

Remember that when you define a custom login view you must make sure that Divio sso login `/login/` is overriden as well, otherwise when a login is required for a page users are redirected to admin login with big `DIVIO` logo.

The best solution is to define in settings.py `LOGIN_URL = reverse_lazy('account_login')` and add to it an aldryn sso button that appears only for admins as following:

```html
{% if '/admin/' in request.GET.next %}
    <a class="btn btn-primary sso-login-btn"
        href="{% url 'aldryn_sso_login' %}{% if request.GET.next %}?next={{ request.GET.next|urlencode }}{% endif %}">
        {% trans "Login with Divio" %}
    </a>
{% endif %}
```
