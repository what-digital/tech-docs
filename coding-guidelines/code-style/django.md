## Module directory structure

- `module_name`
    - `models/` - use one models.py file only if you have 1-2 simple models, otherwise always split them up. Focus on keeping them simple, only data definition logic, little or no data processing logic.
        - `cms_plugins/`
            - `class ListPlugin(CMSPluginBase)`
            - `class BoxPlugin(CMSPluginBase)`
        - `model_1.py`
        - `model_2.py`
    - `views/` - only http request processing logic, everything else (including forms) prefer to keep in `forms` or `services` modules
        - `page1.py`
        - `page2.py`
        - `page3/`
            - `view_1.py`
            - `view_2.py`
        - `mixins.py`
        - `cms_plugins/` - or a single field if there are not more than few simple plugins
            - `list.py`
                - `class ListPlugin`
            - `box.py`
                - `class BoxPlugin`
    - `forms/` - everything related to data parsing and validation, ideally excluding the business logic (not always possible to exclude it in django though)
        - `fields/`
        - `validators/`
    - `services/` - can be everything not related to http requests processing (`views`) or db models (`models`)
        - `price_calc.py`
        - `mail.py`
        - `request_session.py`
    - `templatetags/` - only render related code, everything else move out into other modules, eg `services`
    - `receivers/` - signal listeners
    - `signals.py` - only definitions of your signals, to keep them in one file visible to other developers
    - `helpers.py`/`utils.py` - usually something not related to the app in question, eg a custom python List iterator that you need in this module and have no better place to put it in, split that file up as soon as it has more than 2-4 functions or classes
    - `cms_plugins.py`
        - `class ListPlugin(CMSPluginBase)`
        - `class BoxPlugin(CMSPluginBase)`
    - `templates/`
        - `module_name/`
            - `cms_plugins/`
                - `list.html`
                - `box.html`

## Simple CMS Plugin

- `cms_plugins/`
    - `image_with_background/`
        - `models.py`
            - `class ImageWithBackground`
        - `cms_plugins.py`
            - `class ImageWithBackgroundPlugin`
        - `templates/`
            - `image_with_background/`
                `image_with_background.html`
        - `static/`
            - `image_with_background/`
                `image-with-background.js`
    - `image_with_icon_and_content/`
