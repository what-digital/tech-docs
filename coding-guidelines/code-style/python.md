### Function Signatures

In Python 3.4 always add function signatures.

You can skip adding `-> None` type.

Otherwise yes, always.

Example:
```python
def get_products_by_category(category: str) -> List[dict]:
    pass
```

### Utilize type annotations

Usually a third of the variables declarations are worth covering with type annotations, although it depends on the project complexity. A third is a fine approximation for a simple django project.

Example:
```python
product_boots: Dict[str, str] = product['boots']

class Employee:
    name: str
    id: Tuple[int, str]
```

### Migrations

Give a name to your migrations after you generate them, similar to the git commit. This allows more efficient migration conflict resolutions, along with every other migration-related operation.

### Avoid list comprehension

You can say that there're two kinds of list comprehension, here's the first one:

```python
print([(i, f) for i in nums for f in fruit if f[0] == "P" if i%2 == 1])
```

This is an example from the list comprehension PEP, and it illustrates how frustrating they might be.

And the second kind that can make the code more readable:

```python
candidate_neighbours = [
    neighbour_particle
    for neighbour_particle in contact_network.neighbors(particle_selected)
    if (
        is_quasilinear(particle_selected, neighbour_particle) and
        is_quasilinear(neighbour_particle, particle_selected) and
        is_ahead(particle_selected, neighbour_particle)
    )
]
```

Avoid the first kind. If you don't have time to improve the first kind of list comprehensions - write a simple `for...in`.

And in general be highly careful with list comprehensions. Most of them end up being the hardest to understand parts of the code.

### Always use enums instead of CHOICES, magic numbers and strings

Example:
```python
class CategoryEnum(Enum):
    ADULTS = 'adults'
    KIDS = 'kids'

def get_products_by_category(category: CategoryEnum) -> List[dict]:
    pass
```

For django fields always use django-enumfield packages instead of choices. We prefer this package over Django 3 custom enums as they appears to be incompatible with python native enumerations.

### Line Length

Generally stick to 80-100 character.

If more seems to be needed - most often it's a complicated statements that needs to be broken down into more readable set of instructions, a real example:
```python
def regions_with_permissions(self):
    return (('', '---------'),) + tuple((key, val) for key, val in settings.REGIONS.items() if val in self.current_user.groups.values_list('name', flat=True))
```

One-liners might appear elegant when we write them, but one needs to remember that writing readable code is not about allowing only smart people to comprehend it, but in making sure that everyone is able to understand it freely without the need to rewrite it from scratch. As I had to do while debugging the aforementioned snippet:

```python
def regions_selectable_by_user_for_html_select_input(self) -> List[tuple]:
    select_input_choices: List[tuple] = []
    for region_id, region_name in settings.REGIONS.items():
        is_user_has_region_access = self.current_user.groups.filter(name=region_name).exists()
        if is_user_has_region_access:
            select_input_choice = (region_id, region_name)
            select_input_choices.append(select_input_choice)

    select_input_empty_choice = (None, '------')
    select_input_choices = [select_input_empty_choice, *select_input_choices]
    return select_input_choices
```

### Use dataclasses

Avoid using dict or tuples for passing complex data, unless you have a good reason for doing that.

Typical examples that introduce unnecessary complexity:
```python
return (False, is_valid, person_list)
```
```python
return {'is_updated': False, 'is_valid': True, 'person_list': person_list}
```

Instead write:
```python
@dataclass
class ValidationResult:
    is_updated: bool
    is_valid: bool
    person_list: List[Person]


def validate_form() -> ValidationResult:
    return ValidationResult(is_updated=False, is_valid=True, person_list=person_list)
```

### Classes' methods order

Place public (or overridden) methods on top in the order of their invocation, place private methods below them again in the order of their invocation and prefixed with `_`.