### General notes

- Always add type annotations at least to your functions' headers
- Always use `===` instead of `==`
- Use `let` for mutable variables and `const` for immutable
- Prefer DOM API over JQuery, for documentation use MDN
- When IE support isn't needed use Template literals for string building by default instead of `'`
- When IE support isn't needed utilize async/await over primitive promises
- Use CamelCase instead of snake_case
- User 4 spaces for indentation
- Keep the type checking integration enabled

### Passing variables from django

Define a global `DJANGO` variable as following:
```html
<script>
    const DJANGO = {
        user: {
            username: `{{ request.user.username }}`,
        }
    }
</script>
```

This way you can create an interface for it in typescript, eg:
```typescript
declare global {
    interface Window {        
        DJANGO: {
            user: {
                username: string
            }
        }
    }
}
```
