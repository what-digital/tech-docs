# terminal

### emulators

Iterm (mac) or Terminator (linux) work perfectly. They allow graceful multi-window management, and a lot more.

### fish + ohmyfish + virtualfish + bobthefish

The only reason I know for why people don't use it is because they stick to zsh, which is kind of backward compatible with bash.

Also virtualfish allows you to activate your python venv when you enter the respective directory. `vf connect` - connects the current dir to the current vf venv, and it's going to auto enable the venv once you enter the dir, and disable it once you leave, only has to be enabled by adding `eval (python3 -m virtualfish auto_activation)` to your fish.config.

A high-quality auto-complete on CTRL+R: `brew install fzf` and then run `/usr/local/opt/fzf/install`.

# browsers

### search engines

I find it helpful to add a custom search engine for stack exchange and overflow, and excluding their english learning subsites.

The custom query is `{google:baseURL}search?q=%s site: site:stackoverflow.com OR site:stackexchange.com -ell.stackexchange.com -english.stackexchange.com`

After adding it in the url input you can just enter `s` + space and it’s going to search for your query on the custom engine.

Also here’s one for MDN, because I’m getting disturbed by W3 being on top of my search results - `{google:baseURL}search?q=%s site:https://developer.mozilla.org`.

### extentions

#### Form Filler
An open source and helps greatly with annoying forms - https://chrome.google.com/webstore/detail/form-filler/bnjjngeaknajbdcgpfkgnonkmififhfo?hl=en.

#### Vimium

This browser extension is great, and easier to learn than it looks.

#### Google Search Filter

Allows to highlight the desired results (stackoverflow, wiki, etc) and block the poorly written sites as w3schools

Config example:
```
+apple.stackexchange.com
+stackoverflow.com
+developer.mozilla.org
+github.com
+en.wikipedia.org
www.w3schools.com
```

https://chrome.google.com/webstore/detail/google-search-filter/eidhkmnbiahhgbgpjpiimdogfidfikgf

It has to request access to all website, as the author claims that google was providing no way of accessing only one website's page. Yet now it allows to right click on the installed extension and enable it on google only.

# intellij (pycharm, webstorm, etc)

There's a shortcut for adding another horizontal tab `Split Vertically`, and a shortcut for switching between them. I use cms+shift+d for splitting, and cmd+] and cmd+[ for switching between right and left.

### plugins

You can enable word wraps only in markdown. Here's [an example](https://d3nmt5vlzunoa1.cloudfront.net/idea/files/2019/02/EditorSoftWrap.png).

### VSC

Intellij merge resolution tool is incomparable with the command line, and well integrated into the IDEA.

The rebasing tool since 2019 became dead simple, can be used from the branches view.

# system tools

### simple editors

When you want to edit a single file but don't want to use nano or few hundred MB of RAM for a node editor:
- on linux there's `kate`
- on mac `mate`
