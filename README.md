
- coding-guidelines
    - code-style
        - [django](/coding-guidelines/code-style/django.md)
        - [python](/coding-guidelines/code-style/python.md)
        - [scss](/coding-guidelines/code-style/scss.md)
        - [typescript](/coding-guidelines/code-style/typescript.md)
    - [django-cms](/coding-guidelines/django-cms.md)
    - [general](/coding-guidelines/general.md)
    - [translations](/coding-guidelines/translations.md)
    - [versioning](/coding-guidelines/versioning.md)
- dev-setup
    - [docker](/dev-setup/docker.md)
    - [vagrant](/dev-setup/vagrant.md)
- divio
    - [divio-cloud-improvements](/divio/divio-cloud-improvements.md)
    - [divio-common-errors](/divio/divio-common-errors.md)
- [tools](/tools.md)


This ToC is automatically generated. For generation instructions 
    look into [generate-readme.py](/generate-readme.py)
