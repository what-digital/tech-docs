# pip-reqs

This is an extension of pip-tools, but it has a set of limitation, which cause server/deployment errors, and the error messages are often related to the source of the problem.

You must be familiar with the list belov:

- It doesn't support git+https or other kind of git links, you have to you .zip export with git.
- Don't try to remove `PIP_INDEX_URL` and `WHEELSPROXY_URL` envs from docker, if you do the divio deployment is going to fail without errors. For the same reason it isn't possible to use pip-tools instead.

## HTTPError: 500 Server Error on deployment

If on deployment you're seeing an aldryn wheel proxy 500 server error as following:

`pip._vendor.requests.exceptions.HTTPError: 500 Server Error: Internal Server Error for url: `[`https://wheels.aldryn.net/v1/aldryn-extras+pypi/aldryn-baseproject-v4-py36/+resolve/`](https://wheels.aldryn.net/v1/aldryn-extras+pypi/aldryn-baseproject-v4-py36/+resolve/)

Most likely divio custom pypi server doesn't have the actual list of pypi.org packages

This issue might go away in few days, or it might not and you will have to use a zip archive release installation.

For example pypi.org might have django 1.11.29, and divio repository only 1.11.28. It might happen for a number of reasons, for example there used to be a bug that prevented wheels building, and since divio platform seems like it uses wheels only - when it failed to build a wheel it skipped that version version.

Also remember that divio platform uses pypi caching, hence if you want to use a branch zip archive - it won't be pulling the latest commits in time, even for a month old releases.
